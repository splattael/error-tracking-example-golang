FROM golang

# ENV ERROR_TRACKING_DSN=https://glet_203exxxxh3@gitlab.com/api/v4/error_tracking/collector/19618667

WORKDIR /go/src/app

RUN go version
RUN go env -w GO111MODULE=off
COPY *.go .
RUN go get -d -v ./...
RUN go build -o error

CMD [ "./error" ]
