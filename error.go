package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
)

func main() {
	dsn, ok := os.LookupEnv("ERROR_TRACKING_DSN")
	if !ok {
		panic("Missing envvar ERROR_TRACKING_DSN")
	}

	if err := sentry.Init(sentry.ClientOptions{
		Dsn:   dsn,
		Debug: true,
	}); err != nil {
		fmt.Printf("Sentry initialization failed: %v\n", err)
	}
	log.Println(sentry.CaptureException(fmt.Errorf("test")))
	log.Println(sentry.Flush(1 * time.Second))
}
